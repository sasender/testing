FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
#RUN mvn package
RUN mvn package && ls -l /tmp && ls -l /tmp/target
#RUN mvn clean package
#FROM tomcat:9.0-jre8-alpine
FROM tomcat:8.0-alpine
WORKDIR /usr/local/tomcat/webapps
#RUN rm -rf /usr/local/tomcat/webapps/*
COPY --from=MAVEN_TOOL_CHAIN /tmp/target/my-app-1.0-SNAPSHOT.jar .
EXPOSE 8080
CMD ["catalina.sh", "run"]
HEALTHCHECK --interval=1m --timeout=3s CMD wget --quiet --tries=1 --spider http://localhost:8080/wizard/ || exit
